﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BestScore : MonoBehaviour {

	public Text highscoreDisplay;


	// Use this for initialization
	void Start () {
		highscoreDisplay = GetComponent<Text> ();

		//setting the last known highscore when the game begins
		highscoreDisplay.text = "Best: " + PlayerPrefs.GetInt ("HighScore", 0).ToString();
	}


}
