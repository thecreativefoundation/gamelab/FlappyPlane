﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCount : MonoBehaviour {

	public Text HighscoreDisplay;

	private int score = 0;
	private Text scoreDisplay;

	// Use this for initialization
	void Start () {
		scoreDisplay = GetComponent<Text> ();
	}

	public void IncrementScore(){
		score++;
		if (score > PlayerPrefs.GetInt ("HighScore", 0)) {
			PlayerPrefs.SetInt ("HighScore", score);
			HighscoreDisplay.text = score.ToString();
		}
		scoreDisplay.text = "Score: " + score.ToString();
	}
}
