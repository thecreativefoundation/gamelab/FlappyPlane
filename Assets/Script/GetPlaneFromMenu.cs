﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetPlaneFromMenu : MonoBehaviour {

	public Sprite[] Planes;
	private SpriteRenderer currentPlane;
	private int planeChoice;
	private Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();

		currentPlane = GetComponent<SpriteRenderer> ();
		planeChoice = PlayerPrefs.GetInt ("PlaneChoice");
		currentPlane.sprite = Planes [planeChoice];

		PlaneSelection ();
	}

	public void PlaneSelection(){
		if (planeChoice == 0) {
			animator.SetTrigger ("PlaneGreen_Trigger");
		}else if(planeChoice ==1){
			animator.SetTrigger ("PlaneRed_Trigger");
		}
	}

}
