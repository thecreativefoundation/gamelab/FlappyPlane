﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    private float floorValue = -2,roofValue = 2;

    private void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Spike"))
        {
            float formationHeight = Random.Range(floorValue, roofValue);
            Vector2 formationPosition = new Vector2(3.35f, formationHeight);	//conversion into vector
            other.transform.parent.position = formationPosition;
        }
    }
}
